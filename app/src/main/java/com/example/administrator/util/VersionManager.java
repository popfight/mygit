package com.example.administrator.util;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.RemoteViews;
import android.widget.TextView;
import android.widget.Toast;

import com.example.administrator.mygit.R;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by chenyan on 2015/9/1.
 */
public class VersionManager
{
    private static Context context = null;
    private static Dialog upgrateDlg = null;

    // app下载通知栏
    private static NotificationManager notifyManager = null;
    private static Notification appNotification = null;
    private static long lastNotifyTime = 0;

    @SuppressLint("HandlerLeak")
    private static Handler progressHandler = new Handler()
    {
        public void handleMessage(Message msg)
        {
            super.handleMessage(msg);

            if (notifyManager != null && appNotification != null)
            {
                if (msg.what < 100)
                {
                    // 下载未完成>>显示进度条
//                    appNotification.contentView.setTextViewText(R.id.content_view_title, "91拍: 已下载 " + msg.what + "%");
//                    appNotification.contentView.setProgressBar(R.id.content_view_progress, 100, msg.what, false);
                }
                else
                {
                    // 下载完成>>显示完成提示
//                    appNotification.contentView.setTextViewText(R.id.content_view_title, "91拍: 下载完成");
//                    appNotification.contentView.removeAllViews(R.id.content_view_progress_layout);
                }

                notifyManager.notify(0, appNotification);
            }
        }
    };

    public static void setContext(Context _context)
    {
        context = _context;
    }
    // 获取应用当前版本名
    public static String getAppVersionName()
    {
        try
        {
            PackageManager pm = context.getPackageManager();
            PackageInfo pi = pm.getPackageInfo(context.getPackageName(), PackageManager.GET_CONFIGURATIONS);
            return pi.versionName;
        }
        catch (PackageManager.NameNotFoundException e)
        {
            return "";
        }
    }

    // 更新升级
    public static void updateApp()
    {
        try
        {
            showUpgrateDialog();
        }
        catch(Exception e)
        {
            Toast.makeText(context, "更新过程出错...", Toast.LENGTH_SHORT).show();
        }
    }

    private static void showUpgrateDialog()
    {
//        upgrateDlg = new Dialog(context, R.style.roundedDialog);
//        upgrateDlg.setContentView(R.layout.delete_confirm_dialog);
//        TextView title = (TextView)upgrateDlg.findViewById(R.id.delete_confirm_title);
//        TextView upgrateBtn = (TextView)upgrateDlg.findViewById(R.id.delete_confirm_yes);
//        TextView cancelBtn = (TextView)upgrateDlg.findViewById(R.id.delete_confirm_cancel);
//        title.setText("检测到最新版本，请下载更新...");
//        upgrateBtn.setText("更新");
//
//        upgrateBtn.setOnClickListener(new View.OnClickListener()
//        {
//            @Override
//            public void onClick(View v)
//            {
//                notifyManager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
//                appNotification = new Notification();
//                appNotification.icon = R.drawable.desktop_icon;
//                appNotification.tickerText = "下载通知";
//                appNotification.contentView = new RemoteViews(context.getPackageName(), R.layout.notify_download_advapp);
//                notifyManager.notify(0, appNotification);
//
//                new Thread()
//                {
//                    @Override
//                    public void run()
//                    {
//                        try
//                        {
//                            String url = "http://www.91pai.net/apk/"+R.string.app_name+".apk";
//                            downloadApk(url);
//                        }
//                        catch(Exception e)
//                        {
//                            Toast.makeText(context, "更新过程出错啦...", Toast.LENGTH_SHORT).show();
//                        }
//                    }
//                }.start();
//
//                upgrateDlg.dismiss();
//            }
//        });
//
//        cancelBtn.setOnClickListener(new View.OnClickListener()
//        {
//            @Override
//            public void onClick(View v)
//            {
//                upgrateDlg.dismiss();
//            }
//        });
//
//        upgrateDlg.show();
    }
    // 安装apk
    public static void installApk(File file)
    {
//        Intent intent = new Intent();
//        intent.setAction(Intent.ACTION_VIEW);
//        intent.setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive");
//        MainActivity.mainActivity.startActivity(intent);
    }

    // 从服务器下载apk
    public static void downloadApk(String path) throws Exception
    {
//        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED))
//        {
//            URL url = new URL(path);
//            HttpURLConnection conn = (HttpURLConnection)url.openConnection();
//            conn.setConnectTimeout(5000);
//
//            // 获取到的文件大小
//            File apkDir = new File(XConstant.ADVAPK_CACHE_FILE_PATH);
//            if (!apkDir.exists())
//            {
//                apkDir.mkdir();
//            }
//            else if (!apkDir.isDirectory())
//            {
//                apkDir.mkdir();
//            }
//
//            long fileLength = conn.getContentLength();
//            InputStream inputStream = conn.getInputStream();
//            File file = new File(XConstant.ADVAPK_CACHE_FILE_PATH + "91pai.apk");
//            FileOutputStream fos = new FileOutputStream(file);
//            BufferedInputStream bis = new BufferedInputStream(inputStream);
//            byte[] buffer = new byte[1024];
//            int len = 0;
//            long total = 0;
//            while ((len = bis.read(buffer, 0, 1024)) != -1)
//            {
//                fos.write(buffer, 0, len);
//                total += len;
//
//                // 每隔1秒钟,通知一次
//                if (System.currentTimeMillis() - lastNotifyTime > 1000)
//                {
//                    Message msg = progressHandler.obtainMessage();
//                    int progress = (int)(total * 100 / fileLength);
//                    msg.what = progress;
//                    progressHandler.sendMessage(msg);
//                    lastNotifyTime = System.currentTimeMillis();
//                }
//            }
//
//            // 下载完成
//            if (total == fileLength)
//            {
//                Message msg = progressHandler.obtainMessage();
//                msg.what = 100;
//                progressHandler.sendMessage(msg);
//
//                // 替换安装
//                installApk(file);
//            }
//
//            fos.close();
//            bis.close();
//            inputStream.close();
//        }// if
    }
}
